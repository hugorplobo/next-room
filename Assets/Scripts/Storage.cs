using System.IO;
using System.Collections.Generic;
using UnityEngine;

public class Storage : MonoBehaviour {
    public UserInformation Read()
    {
        string fileContent = PlayerPrefs.GetString("information", JsonUtility.ToJson(
            new UserInformation(0, 0, 0, 150, new List<Monster>())
        ));

        return JsonUtility.FromJson<UserInformation>(fileContent);
    }

    public void Write(UserInformation userInformation)
    {
        PlayerPrefs.SetString("information", JsonUtility.ToJson(userInformation));
    }
}