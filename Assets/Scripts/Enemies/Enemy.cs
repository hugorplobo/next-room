public interface Enemy
{
    public string GetName();
    public string GetTip();
}
