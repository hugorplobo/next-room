using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Watchers : MonoBehaviour, Enemy
{
    private GameObject player;

    void Start()
    {
        this.player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        if (PauseMenu.isPaused) {
            return;
        }

        Vector3 direction = (this.transform.position - this.player.transform.position).normalized;

        if (Vector3.Dot(direction, this.player.transform.forward) > 0.5f)
        {
            this.player.GetComponent<PlayerStats>().TakeDamage(1, this, false);
            Medical.playerLife -= 1;
        }
    }

    public string GetName()
    {
        return "Watchers";
    }

    public string GetTip()
    {
        return "Tente nao olhar de volta para eles...";
    }
}
