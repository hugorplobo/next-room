using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Muffler : MonoBehaviour, Enemy
{
    public GameObject monsterPrefab;
    private float despawnTime = 4f;
    private GameObject player;
    private GameObject spawnedMonster;
    public AudioClip spawnSound;
    public AudioClip damageSound;
    public AudioClip vanishSound;

    private float initialDistance;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player"); 
        float delayMinutes = Random.Range(1.0f, 2.0f);
        float delaySeconds = delayMinutes * 60.0f;
        StartCoroutine(SpawnMonstersWithDelay(delaySeconds));
    }

    private IEnumerator SpawnMonstersWithDelay(float delaySeconds)
    {
        yield return new WaitForSeconds(delaySeconds);

        while (true)
        {
            if (spawnedMonster == null) 
                Spawn();
                Debug.Log("Spawnando Muffler");

            float nextDelayMinutes = Random.Range(2.0f, 3.0f);
            float nextDelaySeconds = nextDelayMinutes * 60.0f;
            yield return new WaitForSeconds(nextDelaySeconds);
        }
    }

    private void Spawn()
    {
        if (PauseMenu.isPaused) {
            return;
        }
        
        Vector3 spawnPosition = Random.insideUnitSphere * 1;
        spawnPosition += player.transform.position;
        spawnPosition.y = Mathf.Clamp(spawnPosition.y, 1, 2);

        spawnedMonster = Instantiate(monsterPrefab, spawnPosition, Quaternion.identity);
        AudioSource.PlayClipAtPoint(spawnSound, spawnPosition);

        initialDistance = Vector3.Distance(spawnPosition, player.transform.position);

        StartCoroutine(DealDamageAfterDespawn());
    }

    private void Update()
    {
        if (spawnedMonster != null)
        {
            Vector3 direction = player.transform.position - spawnedMonster.transform.position;
            direction.y = 0f;
            float distance = Vector3.Distance(spawnedMonster.transform.position, player.transform.position);

            Quaternion rotation = Quaternion.LookRotation(direction);
            spawnedMonster.transform.rotation = rotation;

            if (distance > initialDistance)
            {
                float moveSpeed = 5f;
                spawnedMonster.transform.position += direction.normalized * moveSpeed * Time.deltaTime;
            }

            direction = (spawnedMonster.transform.position - player.transform.position).normalized;

            if (Vector3.Dot(direction, player.transform.forward) > 0.5f)
            {
                DestroyMonster();
            }
        }
    }


    private IEnumerator DealDamageAfterDespawn()
    {
        yield return new WaitForSeconds(despawnTime);

        if (spawnedMonster != null)
        {
            AudioSource.PlayClipAtPoint(damageSound, spawnedMonster.transform.position);
            GameObject.FindGameObjectWithTag("Player")
                .GetComponent<PlayerStats>()
                .TakeDamage(10, this, false);
            Medical.playerLife -= 10;
            Destroy(spawnedMonster);
        }
    }

    private void DestroyMonster()
    {
        if (spawnedMonster != null)
        {
            Debug.Log("Muffler Morto");
            AudioSource.PlayClipAtPoint(vanishSound, spawnedMonster.transform.position);
            Destroy(spawnedMonster);
        }
    }

    public string GetName()
    {
        return "Muffler";
    }

    public string GetTip()
    {
        return "Talvez você deva procurar a fonte de barulhos ao seu redor...";
    }
}
