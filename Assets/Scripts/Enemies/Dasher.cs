using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dasher : MonoBehaviour, Enemy
{
    public float speed = 0.0f;
    public AudioClip jumpScare;
    public Animator animator;
    private Room currentRoom;

    public void Start()
    {
        GetComponentInChildren<MeshRenderer>().enabled = false;
        this.currentRoom = GameObject.FindGameObjectWithTag("GameController")
            .GetComponent<GameController>()
            .GetCurrentRoom();
    }

    public void Update()
    {
        if (PauseMenu.isPaused) {
            return;
        }

        this.transform.Translate(Vector3.forward * this.speed * Time.deltaTime);

        if ((this.transform.position - this.currentRoom.outDoor.position).magnitude < 1.0f)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals("Lamp"))
        {
            this.HandleLightCollision(other.gameObject.GetComponent<Lamp>());
        } else if (other.tag.Equals("Direction"))
        {
            this.HandleDirectionCollision(other.transform);
        } else if (other.tag.Equals("Player"))
        {
            this.HandlePlayerCollision();
        }
    }

    private void HandleLightCollision(Lamp lamp)
    {
        if (lamp != null)
        {
            lamp.Break();
        }
    }

    private void HandleDirectionCollision(Transform dir)
    {
        if ((this.transform.position - dir.position).magnitude < 1.0f)
        {
            this.transform.rotation = Quaternion.LookRotation(dir.forward);
        }
    }

    private void HandlePlayerCollision()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<PlayerMove>().BlockMove();
        player.GetComponentInChildren<PlayerLook>().LockMouse();

        GameObject flashlight = GameObject.FindGameObjectWithTag("Flashlight");
        Destroy(flashlight);

        GetComponentInChildren<MeshRenderer>().enabled = true;
        Destroy(GetComponentInChildren<ParticleSystem>());

        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.Stop();

        player.transform.Translate(Vector3.up * 1000.0f);
        this.transform.Translate(Vector3.up * 1000.0f);

        this.speed = 0.0f;
        this.transform.position = new Vector3(
            player.transform.position.x,
            this.transform.position.y,
            player.transform.position.z
        );

        this.transform.position += player.transform.forward * 5.0f;

        this.animator.SetTrigger("IsAttacking");
    }

    private void OnJumpScareSpike()
    {
        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.clip = this.jumpScare;
        audioSource.Play();
    }

    private void OnAnimationEnded()
    {
        GameObject.FindGameObjectWithTag("Player")
            .GetComponent<PlayerStats>()
            .TakeDamage(99999, this, false);
        Medical.playerLife = 0;
    }

    public string GetName()
    {
        return "Dasher";
    }

    public string GetTip()
    {
        return "Talvez ele nao consiga te ver dentro dos armarios...";
    }
}
