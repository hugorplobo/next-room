using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour, Enemy
{
    public float speed;
    public new SpriteRenderer renderer;
    private GameObject player;
    private bool isMoving = false;

    public void Start()
    {
        this.player = GameObject.FindGameObjectWithTag("Player");
        this.renderer.enabled = false;
        StartCoroutine(this.DelayMove());
    }

    public void Update()
    {
        if (PauseMenu.isPaused) {
            return;
        }

        if (this.isMoving)
        {
            if (!this.renderer.isVisible)
            {
                this.transform.Translate(Vector3.forward * this.speed * Time.deltaTime);
            }

            if ((this.transform.position - this.player.transform.position).magnitude < 5.0f)
            {
                this.renderer.enabled = true;
            } else {
                this.renderer.enabled = false;
            }
        }
    }

    private IEnumerator DelayMove()
    {
        yield return new WaitForSeconds(15.0f);
        this.isMoving = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            StartCoroutine(this.HandlePlayerCollision());
        } else if (other.tag.Equals("Direction"))
        {
            this.HandleDirectionCollision(other.transform);
        }
    }

    private void HandleDirectionCollision(Transform dir)
    {
        if ((this.transform.position - dir.position).magnitude < 0.2f)
        {
            this.transform.rotation = Quaternion.LookRotation(dir.forward);
        }
    }

    private IEnumerator HandlePlayerCollision()
    {
        if (this.isMoving)
        {
            this.SetPlayerAnimation();

            yield return new WaitForSeconds(0.5f);

            this.player.GetComponent<PlayerStats>()
                .TakeDamage(9999, this, false);

            Medical.playerLife = 0;
        }
    }

    private void SetPlayerAnimation()
    {
        this.player.GetComponent<Animator>().SetTrigger("IsFollowerAttacking");
    }

    public string GetName()
    {
        return "Follower";
    }

    public string GetTip()
    {
        return "Se continuar devagar assim, ele sempre vai chegar em voce...";
    }

    public void SetIsMoving(bool isMoving)
    {
        this.isMoving = isMoving;
    }
}
