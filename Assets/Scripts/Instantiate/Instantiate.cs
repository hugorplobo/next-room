using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class Instantiate : MonoBehaviour
{
    public GameObject bandage;
    public GameObject flashLight;
    public GameObject medKit;
    public GameObject battery;
    void Start()
    {
        this.InstantiateObject();
    }

    private void InstantiateObject()
    {
        float probability = Random.Range(0f, 100f);
        GameObject obj = null;

        if (probability > 25f && probability < 35f)
        {
            obj = Instantiate(this.bandage, this.gameObject.transform.position, this.bandage.transform.rotation);
        } else if (probability > 7f && probability < 10f)
        {
            obj = Instantiate(this.medKit, this.gameObject.transform.position, this.medKit.transform.rotation);
        } else if (probability > 0f && probability < 7f)
        {
            obj = Instantiate(this.flashLight, this.gameObject.transform.position, this.flashLight.transform.rotation);
        } else if (probability > 10f && probability < 15f)
        {
            obj = Instantiate(this.battery, this.gameObject.transform.position, this.battery.transform.rotation);
        }

        if (obj != null) obj.transform.parent = this.transform;
    }
}
