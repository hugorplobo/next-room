using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public AudioSource damageAudio;
    public BarContainer healthBar;
    public int life;

    public void TakeDamage(int damageTaken, Enemy enemy, bool playSound = true)
    {
        this.life -= damageTaken;
        this.healthBar.SetProgress(Mathf.Max(0, this.life));

        if (playSound)
        {
            this.damageAudio.Play();
        }

        if (this.life <= 0)
        {
            GameObject game = GameObject.FindGameObjectWithTag("GameController");
            GameController gc = game.GetComponent<GameController>();            

            gc.SetKiller(enemy);

            Storage storage = game.GetComponent<Storage>();

            UserInformation info = storage.Read();

            info.AddDeath(enemy.GetName());
            info.SetHighestRoom(gc.GetCurrentRoom().id);
            info.sensitivity = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerLook>().sensitivity;

            storage.Write(info);

            game.GetComponent<SceneController>().ChangeToGameOver();
        }
    }

    public int Heal(int life)
    {
        this.life += life;

        this.healthBar.SetProgress(Mathf.Max(0, this.life + life));

        return this.life;
    }
}
