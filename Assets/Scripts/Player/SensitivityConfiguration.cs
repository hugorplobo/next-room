using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SensitivityConfiguration : MonoBehaviour
{
    public Slider sensitivitySlider;
    private PlayerLook playerLook;

    void Start()
    {
        this.sensitivitySlider.value = GameObject
            .FindGameObjectWithTag("MainCamera")
            .GetComponent<PlayerLook>().sensitivity;

        this.sensitivitySlider.onValueChanged.AddListener((v) => {
            GameObject.FindGameObjectWithTag("Player")
            .GetComponentInChildren<PlayerLook>().UpdateSensitivity(v);
        });
    }
}
