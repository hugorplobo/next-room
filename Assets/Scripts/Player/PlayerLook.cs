using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook : MonoBehaviour
{
    public Transform playerBody;
    public float sensitivity = 200.0f;
    private float xRotation = 0.0f;
    private bool isLocked = false;

    public void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        this.sensitivity = GameObject.FindGameObjectWithTag("GameController")
            .GetComponent<Storage>().Read().sensitivity;
    }

    public void Update()
    {
        if (PauseMenu.isPaused) {
            return;
        }

        if (!this.isLocked)
        {
            float mouseX = Input.GetAxis("Mouse X") * this.sensitivity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * this.sensitivity * Time.deltaTime;

            this.xRotation -= mouseY;
            this.xRotation = Mathf.Clamp(this.xRotation, -90f, 90f);

            this.transform.localRotation = Quaternion.Euler(this.xRotation, 0.0f, 0.0f);
            this.playerBody.Rotate(Vector3.up * mouseX);
        }
    }

    public void LockMouse()
    {
        this.isLocked = true;
    }

    public void ReleaseMouse()
    {
        this.isLocked = false;
    }

    public void UpdateSensitivity(float newSensitivity)
    {
        this.sensitivity = newSensitivity;
    }
}
