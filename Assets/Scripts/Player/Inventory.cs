using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    private bool usingItem;
    private Flashlight flashlight;
    public Image flashlightImage;
    public BarContainer itemBattery;

    public void Start()
    {
        this.flashlight = null;
        this.usingItem = false;
        this.itemBattery.SetProgress(0);
        this.itemBattery.gameObject.SetActive(false);
        this.flashlightImage.gameObject.SetActive(false);
    }

    public void Update()
    {
        if (PauseMenu.isPaused)
        {
            return;
        }

        if (this.flashlight == null)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0) && this.usingItem)
        {
            this.usingItem = false;

            this.flashlight.Deactivate();
        } else if (Input.GetMouseButtonDown(0) && !this.usingItem)
        {
            this.usingItem = true;

            this.flashlight.Activate();
        }

        if (this.flashlight != null)
        {
            this.itemBattery.SetProgress(this.flashlight.GetBattery());
        }
    }

    public bool HasItem()
    {
        return this.flashlight != null;
    }

    public void SetFlashligh(Flashlight newFlashLight)
    {
        if (this.flashlight == null)
        {
            this.itemBattery.gameObject.SetActive(true);
            this.flashlightImage.gameObject.SetActive(true);
        }

        this.flashlight = newFlashLight;
    }

    public Flashlight getFlashlight()
    {
        return this.flashlight ? this.flashlight : null;
    }
}
