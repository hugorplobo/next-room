using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    public Room room;
    private float timer = 0.0f;
    private float nextBlink = 0.0f;

    public void Start()
    {
        this.SetNewBlinkTime();
    }

    public void Update()
    {
        this.timer += Time.deltaTime;

        if (this.timer >= this.nextBlink)
        {
            this.SetNewBlinkTime();
            this.BlinkLights();
            this.timer = 0.0f;
        }
    }

    private void BlinkLights()
    {
        foreach (Lamp lamp in this.room.lamps)
        {
            lamp.Blink();
        }
    }

    private void SetNewBlinkTime()
    {
        this.nextBlink = Random.Range(10.0f, 20.0f);
    }
}
