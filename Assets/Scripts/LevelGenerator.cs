using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using TMPro;

public class LevelGenerator : MonoBehaviour
{
    public GameObject loading;

    private Room initRoom;
    private Transform lastDoor;
    private Vector3 initForward;
    private Vector3 lastForward;
    private Vector3 penultimateForward;

    public void Start()
    {
        StartCoroutine(Generate());
    }

    IEnumerator Generate()
    {
        yield return this.GenerateFirstRoom();

        int i = 2;
        while (i <= 100)
        {
            int roomId = Random.Range(2, 9);
            AsyncOperationHandle<GameObject> roomHandle =
                Addressables.LoadAssetAsync<GameObject>($"Assets/Prefabs/Rooms/Room {roomId}.prefab");

            yield return roomHandle;

            if (this.GenerateRoom(roomHandle.Result, i))
            {
                i++;
            }
        }

        yield return this.PostGeneration();
    }

    private IEnumerator GenerateFirstRoom()
    {
        AsyncOperationHandle<GameObject> roomHandle =
            Addressables.LoadAssetAsync<GameObject>($"Assets/Prefabs/Rooms/Room 1.prefab");
        
        yield return roomHandle;

        GameObject room = Instantiate(roomHandle.Result);

        Room roomData = room.GetComponent<Room>();
        roomData.SetId(1);

        this.initRoom = roomData;
        room.transform.position = Vector3.zero;
        this.lastDoor = roomData.outDoor;
        this.initForward = this.lastDoor.forward;
        this.lastForward = this.initForward;
        this.penultimateForward = this.lastForward;
    }

    private bool GenerateRoom(GameObject roomObj, int id)
    {
        GameObject room = Instantiate(roomObj);

        Room roomData = room.GetComponent<Room>();
        roomData.SetId(id);
        room.transform.position = this.lastDoor.position;
        room.transform.rotation = Quaternion.LookRotation(this.lastDoor.forward);

        if (roomData.outDoor.forward == -this.initForward || roomData.outDoor.forward == -this.lastForward || roomData.outDoor.forward == -this.penultimateForward)
        {
            Destroy(room);
            return false;
        }

        Vector3 dist = roomData.entryDoor.position - room.transform.position;
        Vector3 localDist = room.transform.InverseTransformDirection(dist);
        room.transform.Translate(-localDist);
        room.transform.Translate(Vector3.forward * 0.1f);

        this.lastDoor = roomData.outDoor;
        this.penultimateForward = this.lastForward;
        this.lastForward = roomData.outDoor.forward;
        
        return true;
    }

    private IEnumerator PostGeneration()
    {
        AsyncOperationHandle<GameObject> playerHandle =
                Addressables.LoadAssetAsync<GameObject>($"Assets/Prefabs/Player/Player.prefab");
        
        AsyncOperationHandle<GameObject> followerHandle =
                Addressables.LoadAssetAsync<GameObject>($"Assets/Prefabs/Enemies/Follower/Follower.prefab");

        yield return playerHandle;
        yield return followerHandle;

        GameObject player = Instantiate(playerHandle.Result);
        player.transform.position = Vector3.zero;

        GameObject follower = Instantiate(followerHandle.Result);
        follower.transform.position = this.initRoom.entryDoor.position;
        follower.transform.rotation = Quaternion.LookRotation(this.initRoom.entryDoor.forward);

        Destroy(this.loading);
    }
}
