using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drawer : LookTrigger
{
    public Animator animator;
    protected bool isOpened = false;

    private void Start()
    {
        if (this.gameObject.transform.GetChild(0).transform.childCount > 0)
        {
            this.gameObject.transform.GetChild(0).transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && this.isPlayerLooking && !this.isOpened)
        {
            this.ShowMessage("");
            this.animator.SetBool("IsOpened", true);
            this.isOpened = true;
            
            if (this.gameObject.transform.GetChild(0).transform.GetChild(0) != null)
            {
                StartCoroutine(this.EnableChildCollider());
            }
        }
    }

    public override void OnPlayerLook()
    {
        base.OnPlayerLook();

        if (!this.isOpened)
        {
            this.ShowMessage("[E] Abrir");
        }
    }

    public override void OnPlayerStopLook()
    {
        base.OnPlayerStopLook();
        this.ShowMessage("");
    }

    protected void PlayAudio()
    {
        GetComponent<AudioSource>().Play();
    }

    private IEnumerator EnableChildCollider()
    {
        yield return new WaitForSeconds(1.2f);
        this.gameObject.transform.GetChild(0).transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
    }
}
