using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lamp : MonoBehaviour
{
    public new AudioSource audio;
    public new Light light;

    public void Break()
    {
        this.audio.Play();
        this.light.intensity = 0.0f;
        Destroy(this);
    }

    public void TurnOff()
    {
        this.light.intensity = 0.0f;
    }

    public void TurnOn()
    {
        this.light.intensity = 1.0f;
    }

    public void Blink()
    {
        StartCoroutine(this.BlinkCoroutine());
    }

    private IEnumerator BlinkCoroutine()
    {
        this.light.intensity = 0.0f;
        yield return new WaitForSeconds(Random.Range(0.05f, 0.1f));
        this.light.intensity = 1.0f;
        yield return new WaitForSeconds(Random.Range(0.05f, 0.1f));
        this.light.intensity = 0.0f;
        yield return new WaitForSeconds(Random.Range(0.05f, 0.1f));
        this.light.intensity = 1.0f;
    }
}
