using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class Button : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public TextMeshProUGUI text;

    public void OnPointerEnter(PointerEventData eventData)
    {
        this.text.color = Color.red;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        this.text.color = Color.white;
    }
}
