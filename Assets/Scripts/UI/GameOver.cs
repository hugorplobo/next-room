using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOver : MonoBehaviour
{
    public TextMeshProUGUI killerName;
    public TextMeshProUGUI tip;

    void Start()
    {
        PauseMenu.isPaused = false;

        GameController game = GameObject.FindGameObjectWithTag("GameController")
            .GetComponent<GameController>();

        this.killerName.text = $"Voce foi morto por {game.GetKiller().GetName()}";
        this.tip.text = game.GetKiller().GetTip();

        Cursor.lockState = CursorLockMode.None;
    }
}
