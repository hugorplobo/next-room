using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HUD : MonoBehaviour
{
    public TextMeshProUGUI feedbackMessage;
    private Inventory inventory;

    public void Start()
    {
        this.inventory = GameObject.FindGameObjectWithTag("Inventory")
            .GetComponent<Inventory>();
    }

    public void ShowFeedbackMessage(string message)
    {
        this.feedbackMessage.text = message;
    }
}
