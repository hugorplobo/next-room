using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarContainer : MonoBehaviour
{
    public Slider slider;
    public Image progress;
    public Image leftIcon;
    public Color progressColor;
    public int maxProgress;

    public void Start() {
        this.progress.color = this.progressColor;
        this.slider.maxValue = this.maxProgress;
    }

    public void SetProgress(float newProgress) {
        this.slider.value = newProgress;
    }
}
