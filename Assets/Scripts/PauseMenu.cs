using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenu;
    public static bool isPaused { get; set; }
    private SceneController sceneController;

    void Start()
    {
        this.sceneController = GameObject.FindGameObjectWithTag("GameController")
            .GetComponent<SceneController>();
    
        PauseMenu.isPaused = false;
        pauseMenu.SetActive(false);
    }

    void Update()
    {
        if (!Input.GetKeyDown(KeyCode.Escape)) {
            return;
        }

        if (PauseMenu.isPaused)
        {
            this.ResumeGame();
        } else
        {
            this.PauseGame();
        }
    }

    public void PauseGame()
    {
        this.pauseMenu.SetActive(true);

        Cursor.lockState = CursorLockMode.None;

        Time.timeScale = 0f;
        PauseMenu.isPaused = true;
    }

    public void ResumeGame()
    {
        pauseMenu.SetActive(false);

        Time.timeScale = 1f;
        PauseMenu.isPaused = false;

        Cursor.lockState = CursorLockMode.Locked;
    }

    public void GoToMainMenu()
    {
        this.SaveInfo();

        Time.timeScale = 1f;

        this.sceneController.ChangeToMenu();
    }

    public void QuitGame()
    {
        this.SaveInfo();

        this.sceneController.ExitGame();
    }

    private void SaveInfo()
    {
        Storage storage = GameObject.FindGameObjectWithTag("GameController")
            .GetComponent<Storage>();

        UserInformation info = storage.Read();

        info.sensitivity = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerLook>().sensitivity;

        storage.Write(info);
    }
}
