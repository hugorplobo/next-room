using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    public int id;
    public bool hasCloset;
    public Transform entryDoor;
    public Transform outDoor;
    public Door realDoor;
    public FakeDoor[] fakeDoors;
    public Lamp[] lamps;
    private bool hasEntered = false;

    public void SetId(int id)
    {
        this.id = id;

        if (this.realDoor)
        {
            this.realDoor.SetId(id);
        }

        foreach (FakeDoor fake in this.fakeDoors)
        {
            fake.SetId(++id);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player") && !this.hasEntered)
        {
            GameObject watchers = GameObject.FindGameObjectWithTag("Watchers");
            if (watchers)
            {
                Destroy(watchers);
            }

            GameObject.FindGameObjectWithTag("GameController")
                .GetComponent<GameController>()
                .SetCurrentRoom(this);
            
            this.hasEntered = true;
        }
    }
}
