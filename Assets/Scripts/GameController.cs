using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class GameController : MonoBehaviour
{
    private GameObject dasher;
    private GameObject watchers;
    private Room currentRoom;
    private bool hasSpawned = false;
    private Enemy killer;
    private SceneController sceneController;

    public void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public void Start()
    {
        StartCoroutine(this.LoadEnemies());
        this.sceneController = GetComponent<SceneController>();
    }

    private IEnumerator SpawnDasher()
    {
        if (Random.Range(1, 100) > 50 || this.hasSpawned || !this.currentRoom.hasCloset)
        {
            yield break;
        }

        this.hasSpawned = true;
        Debug.Log("Spawnando Dasher");

        foreach (Lamp lamp in this.currentRoom.lamps)
        {
            lamp.Blink();
        }

        yield return new WaitForSeconds(5.0f);

        if (this.sceneController.GetSceneName() != "Game")
        {
            yield break;
        }

        GameObject dasher = Instantiate(this.dasher);
        dasher.transform.position = this.currentRoom.entryDoor.position;
        dasher.transform.forward = this.currentRoom.entryDoor.forward;
        dasher.transform.Translate(Vector3.back * 10.0f);
    }

    private IEnumerator SpawnWatchers()
    {
        if (Random.Range(1, 100) > 20 || this.hasSpawned)
        {
            yield break;
        }

        this.hasSpawned = true;
        Debug.Log("Spawnando Watchers");

        foreach (Lamp lamp in this.currentRoom.lamps)
        {
            lamp.TurnOff();
        }

        yield return new WaitForSeconds(2.0f);

        if (this.sceneController.GetSceneName() != "Game")
        {
            yield break;
        }

        GameObject watchers = Instantiate(this.watchers);
        watchers.transform.position = this.currentRoom.transform.position;
    }

    private IEnumerator LoadEnemies()
    {
        AsyncOperationHandle<GameObject> dasherHandle =
            Addressables.LoadAssetAsync<GameObject>($"Assets/Prefabs/Enemies/Dasher/Dasher.prefab");
        
        AsyncOperationHandle<GameObject> watchersHandle =
            Addressables.LoadAssetAsync<GameObject>($"Assets/Prefabs/Enemies/Watchers/Watchers.prefab");
        
        yield return dasherHandle;
        yield return watchersHandle;

        this.dasher = dasherHandle.Result;
        this.watchers = watchersHandle.Result;
    }

    public void SetCurrentRoom(Room room)
    {
        if (room.id == 100) {
            this.sceneController.ChangeToWin();

            return;
        }

        Debug.Log($"Entrou na sala {room.id}");
        this.currentRoom = room;

        StartCoroutine(this.SpawnDasher());
        StartCoroutine(this.SpawnWatchers());

        this.hasSpawned = false;
    }

    public Room GetCurrentRoom()
    {
        return this.currentRoom;
    }

    public void SetKiller(Enemy enemy)
    {
        this.killer = enemy;
    }

    public Enemy GetKiller()
    {
        return this.killer;
    }
}
