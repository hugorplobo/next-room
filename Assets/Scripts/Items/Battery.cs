using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battery : LookTrigger
{
    public int batteryValue = 30;
    public static bool hasPlayerFlashlight = false;
    public static float batteryFlashlightValue = 0f;

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.E) && this.isPlayerLooking && hasPlayerFlashlight && batteryFlashlightValue < 180f) 
        {
            this.ShowMessage("");
            batteryFlashlightValue = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Inventory>().getFlashlight().Recharge(this.batteryValue);
            Destroy(this.gameObject);
        }
    }

    public override void OnPlayerLook()
    {
        base.OnPlayerLook();
        

        if (hasPlayerFlashlight && batteryFlashlightValue < 180f)
        {
            this.ShowMessage("[E] Recarregar");
        }
    }

    public override void OnPlayerStopLook()
    {
        base.OnPlayerStopLook();
        this.ShowMessage("");
    }
}
