using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Medical : LookTrigger
{
    public int lifeRecover;
    public static int playerLife = 100;
    private bool used = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && this.isPlayerLooking && !this.used && playerLife < 100)
        {
            this.ShowMessage("");
            playerLife = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>().Heal(this.lifeRecover);
            this.used = true;
            Destroy(this.gameObject);
        }
    }

    public override void OnPlayerLook()
    {
        base.OnPlayerLook();

        if (!this.used && playerLife < 100)
        {
            this.ShowMessage("[E] Usar");
        }
    }

    public override void OnPlayerStopLook()
    {
        base.OnPlayerStopLook();
        this.ShowMessage("");
    }
}
