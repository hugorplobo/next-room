public interface Item
{
    public void Activate();
    public string GetName();
}
