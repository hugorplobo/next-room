using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLightConcrete : LookTrigger
{
    private bool caught = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && this.isPlayerLooking && !this.caught)
        {
            this.ShowMessage("");
            Flashlight flashlight = GameObject.FindGameObjectWithTag("Player").AddComponent<Flashlight>();
            GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Inventory>().SetFlashligh(flashlight);
            Battery.hasPlayerFlashlight = true;
            Battery.batteryFlashlightValue = 180f;
            this.caught = true;
            Destroy(this.gameObject);
        }
    }

    public override void OnPlayerLook()
    {
        base.OnPlayerLook();

        if (!this.caught)
        {
            this.ShowMessage("[E] Pegar");
        }
    }

    public override void OnPlayerStopLook()
    {
        base.OnPlayerStopLook();
        this.ShowMessage("");
    }
}