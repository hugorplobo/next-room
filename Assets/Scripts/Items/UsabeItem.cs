public interface UsableItem : Item
{
    public void Deactivate();
}