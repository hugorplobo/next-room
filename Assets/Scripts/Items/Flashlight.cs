using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : MonoBehaviour, UsableItem
{
    private float battery = 180.0f;
    private new Light light;

    public void Start()
    {
        this.light = GameObject.FindGameObjectWithTag("Flashlight")
            .GetComponent<Light>();
    }

    public void Update()
    {
        if (PauseMenu.isPaused) {
            return;
        }

        if (this.light.intensity == 1.0f)
        {
            this.battery -= Time.deltaTime;
        }

        if (this.battery <= 0.0f)
        {
            this.light.intensity = 0.0f;
        }
    }

    public void Activate()
    {
        if (this.battery > 0.0f && this.light.intensity == 0.0f)
        {
            this.light.intensity = 1.0f;
        } else
        {
            this.light.intensity = 0.0f;
        }
    }

    public void Deactivate()
    {
        this.light.intensity = 0.0f;
    }

    public string GetName()
    {
        return "Lanterna";
    }

    public float GetBattery() {
        return this.battery;
    }

    public float Recharge(float battery)
    {
        if (this.battery + battery > 180f) 
        {
            this.battery = 180f;
        } else
        {
            this.battery += battery;
        }

        return this.battery;
    }
}
