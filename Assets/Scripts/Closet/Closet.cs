using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Closet : LookTrigger, Enemy
{
    public AudioSource audioSource;
    private bool isPlayerInside = false;
    private float timeInside = 0.0f;
    private float maxTime = 5.0f;

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && this.isPlayerLooking && !this.isPlayerInside)
        {
            this.ShowMessage("[E] Sair");
            this.isPlayerInside = true;

            GameObject player = GameObject.FindGameObjectWithTag("Player");
            GetComponent<BoxCollider>().enabled = false;
            player.GetComponent<PlayerMove>().BlockMove();

            GameObject.FindGameObjectWithTag("Follower")
                .GetComponent<Follower>()
                .SetIsMoving(false);

            player.transform.position = this.transform.position;
            player.transform.Rotate(Vector3.up, 180.0f);
            this.audioSource.Play();
        } else if (Input.GetKeyDown(KeyCode.E) && this.isPlayerInside)
        {
            this.ExitPlayer();
        }

        if (this.isPlayerInside)
        {
            this.timeInside += Time.deltaTime;
            this.audioSource.volume = this.timeInside / this.maxTime;

            if (this.timeInside >= this.maxTime)
            {
                this.timeInside = 0.0f;
                this.ExitPlayer();
                GameObject.FindGameObjectWithTag("Player")
                    .GetComponent<PlayerStats>()
                    .TakeDamage(20, this);
                Medical.playerLife -= 20;
            }
        }
    }
    public override void OnPlayerLook()
    {
        base.OnPlayerLook();

        if (!this.isPlayerInside)
        {
            this.ShowMessage("[E] Entrar");
        }
    }

    public override void OnPlayerStopLook()
    {
        base.OnPlayerStopLook();
        this.ShowMessage("");
    }

    private void ExitPlayer()
    {
        this.ShowMessage("");
        this.isPlayerInside = false;

        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<PlayerMove>().ResetMove();
        GetComponent<BoxCollider>().enabled = true;
        player.transform.position = this.transform.position + this.transform.forward * 2.0f;
        player.transform.Rotate(Vector3.up, 180.0f);

        this.timeInside = 0.0f;
        this.audioSource.Stop();

        GameObject.FindGameObjectWithTag("Follower")
                .GetComponent<Follower>()
                .SetIsMoving(true);
    }

    public string GetName()
    {
        return "Boogieman";
    }

    public string GetTip()
    {
        return "Nao o incomode por muito tempo nos armarios...";
    }
}
