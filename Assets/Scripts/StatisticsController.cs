using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using TMPro;

public class StatisticsController : MonoBehaviour
{
    public TextMeshProUGUI statsText;
    public Storage storage;
    public Room room;
    private float timer = 0.0f;
    private float nextBlink = 0.0f;

    public void Start()
    {
        this.SetNewBlinkTime();
        UserInformation info = this.storage.Read();

        int wins = info.wins;
        int highestRoom = info.highestroom;
        int deathsCount = info.deathsCount;
        string nemesis = info.GetNemesis();

        StringBuilder sb = new StringBuilder();

        sb.Append($"Total de vitorias: {wins}\n");    
        sb.Append($"Sala mais longe: {highestRoom}\n");
        sb.Append($"Contagem de mortes: {deathsCount}\n");
        sb.Append($"Nemesis: {nemesis}\n");

        this.statsText.text = sb.ToString();
    }

    public void Update()
    {
        this.timer += Time.deltaTime;

        if (this.timer >= this.nextBlink)
        {
            this.SetNewBlinkTime();
            this.BlinkLights();
            this.timer = 0.0f;
        }
    }

    private void BlinkLights()
    {
        foreach (Lamp lamp in this.room.lamps)
        {
            lamp.Blink();
        }
    }

    private void SetNewBlinkTime()
    {
        this.nextBlink = Random.Range(10.0f, 20.0f);
    }
}
