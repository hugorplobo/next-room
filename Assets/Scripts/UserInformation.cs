using System.Collections.Generic;
using System;

[System.Serializable]
public class Monster {
    public string name;
    public int deathCount;

    public Monster(string name, int deathCount) {
        this.name = name;
        this.deathCount = deathCount;
    }
}

[System.Serializable]
public class UserInformation {
    public int wins;
    public int highestroom;
    public int deathsCount;
    public float sensitivity;
    public List<Monster> monsters;

    public UserInformation(int wins, int highestRoom, int deathsCount, float sensitivity, List<Monster> monsters)
    {
        this.wins = wins;
        this.highestroom = highestRoom;
        this.deathsCount = deathsCount;
        this.sensitivity = sensitivity;
        this.monsters = monsters;
    }

    public void AddDeath(string monsterName)
    {
        this.deathsCount += 1;
        bool found = false;

        foreach (Monster monster in this.monsters)
        {
            if (monster.name == monsterName)
            {
                monster.deathCount += 1;
                found = true;

                break;
            }
        }

        if (!found)
        {
            this.monsters.Add(new Monster(monsterName, 1));
        }
    }

    public void SetHighestRoom(int highestRoom)
    {
        this.highestroom = (int) MathF.Max(this.highestroom, highestRoom);
    }

    public string GetNemesis()
    {
        string nemesis = null;
        int deaths = -1;

        foreach(Monster monster in this.monsters)
        {
            if (monster.deathCount > deaths)
            {
                nemesis = monster.name;
                deaths = monster.deathCount;
            }
        }

        if (nemesis == null)
        {
            return "????";
        }

        return nemesis;
    }
}