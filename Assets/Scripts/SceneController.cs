using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public void ChangeToGame()
    {
        SceneManager.LoadScene("Game");

        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("DontDestroy"))
        {
            Destroy(obj);
        }
    }

    public void ChangeToGameOver()
    {
        SceneManager.LoadScene("GameOver");
    }

    public void ChangeToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void ChangeToStats() {
        SceneManager.LoadScene("Statistics");
    }

    public void ChangeToWin()
    {
        SceneManager.LoadScene("Win");
    }

    public void ExitGame()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

    public string GetSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }
}
