using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Door : LookTrigger
{
    public Animator animator;
    public TextMeshProUGUI text;
    protected bool isOpened = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && this.isPlayerLooking && !this.isOpened)
        {
            this.PlayAudio();
            this.ShowMessage("");
            this.animator.SetBool("IsOpened", true);
            this.isOpened = true;

            this.text.text = "?";
        }
    }

    public void SetId(int id)
    {
        this.text.text = $"{id}";
    }

    public override void OnPlayerLook()
    {
        base.OnPlayerLook();

        if (!this.isOpened)
        {
            this.ShowMessage("[E] Abrir");
        }
    }

    public override void OnPlayerStopLook()
    {
        base.OnPlayerStopLook();
        this.ShowMessage("");
    }

    protected void PlayAudio()
    {
        int audioIndex = Random.Range(0, 2);
        GetComponents<AudioSource>()[audioIndex].Play();
    }
}
