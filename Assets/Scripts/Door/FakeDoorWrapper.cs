using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeDoorWrapper : MonoBehaviour, Enemy
{
    public string GetName()
    {
        return "Mimic";
    }

    public string GetTip()
    {
        return "Assim como voce, parece que ele sempre erra o numero da porta...";
    }

    public void OnAnimationEnded()
    {
        GameObject.FindGameObjectWithTag("Player")
            .GetComponent<PlayerStats>()
            .TakeDamage(40, this);
        Medical.playerLife -= 40;
    }
}
